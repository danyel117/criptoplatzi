import React from 'react';
import 'react-native-gesture-handler';
import { Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CoinsStack from './src/components/coins/CoinsStack';
import colors from './src/res/colors';
import FavoritesStack from './src/components/favorites/FavoriteStack';
const Tabs = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tabs.Navigator tabBarOptions={{ inactiveTintColor: '#fefefe', style: { backgroundColor: colors.blackPearl } }}>
        <Tabs.Screen
          name='Coins'
          component={CoinsStack}
          options={{
            tabBarIcon: ({ size, color }) => (
              <Image style={{ tintColor: color, width: size, height: size }} source={require('./assets/bank.png')} />
            ),
          }}
        />
        <Tabs.Screen
          name='Favorites'
          component={FavoritesStack}
          options={{
            tabBarIcon: ({ size, color }) => (
              <Image style={{ tintColor: color, width: size, height: size }} source={require('./assets/star.png')} />
            ),
          }}
        />
      </Tabs.Navigator>
    </NavigationContainer>
  );
}
