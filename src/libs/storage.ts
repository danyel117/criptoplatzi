import AsyncStorage from '@react-native-community/async-storage';

class Storage {
  static instance = new Storage();

  store = async (key: string, value: string) => {
    try {
      await AsyncStorage.setItem(key, value);
      return true;
    } catch (err) {
      console.log('storage store error', err);
      return false;
    }
  };
  get = async (key: string) => {
    try {
      return await AsyncStorage.getItem(key);
    } catch (err) {
      console.log('storage get error', err);
      throw Error(err);
    }
  };
  multiGet = async (keys: string[]) => {
    try {
      return await AsyncStorage.multiGet(keys);
    } catch (err) {
      console.log('storage multiget error', err);
      throw Error(err);
    }
  };

  getAllKeys = async () => {
    try {
      return await AsyncStorage.getAllKeys();
    } catch (err) {
      console.log('storage getallkeys error', err);
      throw Error(err);
    }
  };

  remove = async (key: string) => {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (err) {
      console.log('storage remove error', err);
      return false;
    }
  };
}

export default Storage;
