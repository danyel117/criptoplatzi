import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import CoinsItem from '../coins/CoinsItem';
import FavoritesEmptyState from './FavoritesEmptyState';
import colors from '../../res/colors';
import Storage from '../../libs/storage';
const FavoritesScreen = (props: any) => {
  const [favorites, setFavorites] = useState([] as object[]);
  useEffect(() => {
    getFavorites();
  }, [props]);

  const getFavorites = async () => {
    try {
      const allKeys = await Storage.instance.getAllKeys();
      const keys = allKeys.filter((key) => key.includes('favorite'));
      const favs = await Storage.instance.multiGet(keys);
      const favoritesArr: object[] = favs.map((fav) => JSON.parse(fav[1] as string));
      setFavorites(favoritesArr);
    } catch (error) {
      console.error('get favorites error', error);
    }
  };

  const handlePress = (coin: any) => {
    props.navigation.navigate('CoinDetail', { coin });
  };
  return (
    <View style={styles.container}>
      {favorites.length === 0 ? (
        <FavoritesEmptyState />
      ) : (
        <FlatList data={favorites} renderItem={({ item }) => <CoinsItem item={item} onPress={() => handlePress(item)} />} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.charade,
    flex: 1,
  },
});

export default FavoritesScreen;
