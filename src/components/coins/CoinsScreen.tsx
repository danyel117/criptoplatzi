import React, { useEffect, useState } from 'react';
import { View, Text, Pressable, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import Http from '../../libs/http';
import CoinsItem from './CoinsItem';
import colors from '../../res/colors';
import CoinsSearch from '../../components/coins/CoinsSearch';

const CoinsScreen = (props: any) => {
  const [coins, setCoins] = useState([]);
  const [allCoins, setAllCoins] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      const res = await Http.instance.get('https://api.coinlore.net/api/tickers/');
      setCoins(res.data);
      setAllCoins(res.data);
      setLoading(false);
    };
    getData();
  }, []);
  const handlePress = (coin: any) => {
    props.navigation.navigate('CoinDetail', { coin });
  };
  const handleSearch = (query: string) => {
    const coinsFiltered = allCoins.filter((coin) => JSON.stringify(coin).toLowerCase().includes(query.toLowerCase()));
    setCoins(coinsFiltered);
  };
  return (
    <View style={styles.container}>
      <CoinsSearch onChange={handleSearch} />
      {loading ? (
        <ActivityIndicator color='red' size='large' style={styles.loader} />
      ) : (
        <FlatList data={coins} renderItem={({ item }: any) => <CoinsItem item={item} onPress={() => handlePress(item)} />} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
  },
  titleText: {
    marginTop: 5,
    color: '#0c0a20',
    textAlign: 'center',
  },
  btn: {
    padding: 8,
    backgroundColor: 'blue',
    borderRadius: 8,
    margin: 16,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
  },
  loader: {
    marginTop: 60,
  },
});

export default CoinsScreen;
