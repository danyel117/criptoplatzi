import React, { useEffect, useState } from 'react';
import { View, Image, Text, SectionList, StyleSheet, Pressable, Alert } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import CoinMarketItem from '../coinDetail/CoinMarketItem';
import Http from '../../libs/http';
import colors from '../../res/colors';
import Storage from '../../libs/storage';

const CoinDetailScreen = (props: any) => {
  const [coinData, setCoinData] = useState({} as any);
  const [markets, setMarkets] = useState([] as any);
  const [isFavorite, setIsFavorite] = useState(false);
  useEffect(() => {
    const { coin } = props.route.params;
    props.navigation.setOptions({ title: coin.symbol });
    getMarkets(coin.id);
    setCoinData(coin);
  }, []);

  useEffect(() => {
    if (Object.keys(coinData).length > 0) {
      getFavorite();
    }
  }, [coinData]);

  const getFavorite = async () => {
    const key = `favorite-${coinData.id}`;
    try {
      const favStr = await Storage.instance.get(key);
      if (favStr != null) {
        setIsFavorite(true);
      }
    } catch (err) {
      console.error(err);
    }
  };

  const getMarkets = async (coinId: string) => {
    const url = `https://api.coinlore.net/api/coin/markets/?id=${coinId}`;
    const markets = await Http.instance.get(url);
    setMarkets(markets);
  };

  const getSymbolIcon = (name: string) => {
    if (name) {
      const symbol = name.toLowerCase().replace(' ', '-');
      return `https://c1.coinlore.com/img/16x16/${symbol}.png`;
    }
  };

  const getSections = (coin: any) => {
    const sections = [
      {
        title: 'Market Cap',
        data: [coin.market_cap_usd],
      },
      {
        title: 'Volume 24h',
        data: [coin.volume24],
      },
      {
        title: 'Change 24h',
        data: [coin.percent_change_24h],
      },
    ];
    return sections;
  };

  const toggleFavorite = async () => {
    const key = `favorite-${coinData.id}`;
    if (isFavorite) {
      await Alert.alert('Remove Favorite', 'Are you sure?', [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Remove',
          onPress: async () => {
            await Storage.instance.remove(key);
            setIsFavorite(false);
          },
          style: 'destructive',
        },
      ]);
    } else {
      const stored = await Storage.instance.store(key, JSON.stringify(coinData));
      if (stored) {
        setIsFavorite(true);
      }
    }
  };

  return (
    <View style={styles.container}>
      {coinData && (
        <>
          <View style={styles.subHeader}>
            <View style={styles.row}>
              <Image style={styles.iconImg} source={{ uri: getSymbolIcon(coinData.name) }} />
              <Text style={styles.titleText}>{coinData.name}</Text>
            </View>
            <Pressable
              onPress={toggleFavorite}
              style={[styles.btnFavorite, isFavorite ? styles.btnFavoriteRemove : styles.btnFavoriteAdd]}
            >
              <Text style={styles.btnFavoriteText}>{isFavorite ? 'Remove from favorites' : 'Add to Favorites'}</Text>
            </Pressable>
          </View>
          <SectionList
            style={styles.section}
            sections={getSections(coinData)}
            keyExtractor={(item) => item}
            renderItem={({ item }) => (
              <View style={styles.sectionItem}>
                <Text style={styles.itemText}>{item}</Text>
              </View>
            )}
            renderSectionHeader={({ section }) => (
              <View style={styles.sectionHeader}>
                <Text style={styles.sectionText}>{section.title}</Text>
              </View>
            )}
          />
          <Text style={styles.marketsTitle}>Markets</Text>
          <FlatList
            keyExtractor={(item: any) => `${item.base}-${item.name}-${item.quote}`}
            style={styles.list}
            horizontal={true}
            data={markets}
            renderItem={({ item }) => <CoinMarketItem item={item} />}
          />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
  },
  subHeader: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 8,
  },
  iconImg: {
    width: 25,
    height: 25,
  },
  section: {
    maxHeight: 220,
  },
  list: {
    maxHeight: 100,
    paddingLeft: 16,
  },
  sectionHeader: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    padding: 8,
  },
  sectionItem: {
    padding: 8,
  },
  itemText: {
    color: '#fff',
    fontSize: 14,
  },
  sectionText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  marketsTitle: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 16,
    marginBottom: 16,
  },
  btnFavorite: {
    padding: 8,
    borderRadius: 8,
  },
  btnFavoriteAdd: {
    backgroundColor: colors.picton,
  },
  btnFavoriteRemove: {
    backgroundColor: colors.carmine,
  },
  btnFavoriteText: {
    color: '#fff',
  },
  row: {
    flexDirection: 'row',
  },
});

export default CoinDetailScreen;
